#!/bin/bash
#SBATCH --job-name=seq
#SBATCH --output=%x.o%j
A = $Nproc
#SBATCH --ntasks=16
#SBATCH --time=00:05:00
#SBATCH --partition=cpu_short           # (see available partitions)

# To clean and load modules defined at the compile and link phases
module purge

module load intel/19.0.3/gcc-4.8.5
module load intel-mpi/2019.3.199/intel-19.0.3.199

make

# execution
# srun ./solver